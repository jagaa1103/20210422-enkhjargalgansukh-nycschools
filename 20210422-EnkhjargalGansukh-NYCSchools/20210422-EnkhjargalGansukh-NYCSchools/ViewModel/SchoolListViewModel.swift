//
//  SchoolListViewModel.swift
//  20210422-EnkhjargalGansukh-NYCSchools
//
//  Created by Enkhjargal Gansukh on 2021.04.22.
//

import Foundation

// Protocol for delegation
protocol SchoolListProtocol {
    func fetched()
}

class SchoolListViewModel {
    
    // variables
    private let networkManager = NetworkManager()
    var delegate: SchoolListProtocol?
    var schools = [School]()
    var showAlert = false
    
    // fetching school list
    func getSchoolList() {
        DispatchQueue.global().async {
            self.networkManager.fetchSchools { result in
                // checking response is successful or failed
                switch result {
                    case .success(let entities):
                        DispatchQueue.main.async {
                            self.schools = entities
                            //known update by delegation
                            self.delegate?.fetched()
                        }
                        break
                    case .failure(_):
                        DispatchQueue.main.async {
                            self.showAlert = true
                        }
                        break
                }
            }
        }
    }
}
