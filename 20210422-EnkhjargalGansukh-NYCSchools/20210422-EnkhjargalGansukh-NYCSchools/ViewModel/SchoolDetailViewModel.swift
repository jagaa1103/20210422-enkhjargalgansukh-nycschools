//
//  SchoolDetailViewModel.swift
//  20210422-EnkhjargalGansukh-NYCSchools
//
//  Created by Enkhjargal Gansukh on 2021.04.22.
//

import Foundation

// Protocol for delegation
protocol SchoolDetailProtocol {
    func fetched()
}

class SchoolDetailViewModel {
    // variables
    let networkManager = NetworkManager()
    var delegate: SchoolDetailProtocol?
    var detailData: SATResult?
    
    
    // fetching school detail about SAT
    func getSchoolDetail(dbn: String) {
        networkManager.fetchSchoolDetailInformation(dbn: dbn) { result in
            // checking response is successful or failed
            switch result {
                case .success(let sat):
                    DispatchQueue.main.async {
                        self.detailData = sat
                        //known update by delegation
                        self.delegate?.fetched()
                    }
                    break
                case .failure(_):
//                    DispatchQueue.main.async {
//                        self.showAlert(error: error)
//                    }
                    break
            }
        }
    }
}
