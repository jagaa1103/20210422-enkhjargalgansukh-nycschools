//
//  ResultError.swift
//  20210422-EnkhjargalGansukh-NYCSchools
//
//  Created by Enkhjargal Gansukh on 2021.04.22.
//

import Foundation

/*
    Error types of data fetching completion
*/

enum ResultError: Error {
    case NetworkError
    case ParsingError
}
