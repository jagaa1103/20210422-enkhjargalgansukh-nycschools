//
//  NetworkManager.swift
//  20210422-EnkhjargalGansukh-NYCSchools
//
//  Created by Enkhjargal Gansukh on 2021.04.22.
//

import Foundation
/*
    NetworkManager is responssible to fetching and parsing all type of data.
*/
struct NetworkManager {
    
    // URL for fetching school list data
    let URL_SCHOOL_LIST = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    // URL for fetching school sat detail data
    let URL_SCHOOL_SAT_DETAIL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    
    //fetching school list by API and respond result type of school array or error
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> ()) {
        guard let url = URL(string: URL_SCHOOL_LIST) else {
            completion(.failure(ResultError.NetworkError))
            return
        }
        // using URLSession to fetch data from API
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let _ = error {
                completion(.failure(ResultError.NetworkError))
                return
            }
            guard let data = data else {
                completion(.failure(ResultError.NetworkError))
                return
            }
            // parsing school list if it's success returns or error happens return error type
            do {
                let entities = try JSONDecoder().decode([School].self, from: data)
                completion(.success(entities))
            } catch {
                completion(.failure(ResultError.ParsingError))
            }
        }
        task.resume()
    }
    
    //fetching school detail by API and respond result type of school's SAT information or error
    func fetchSchoolDetailInformation(dbn: String, completion: @escaping (Result<SATResult?, Error>) -> ()) {
        guard let url = URL(string: "\(URL_SCHOOL_SAT_DETAIL)\(dbn)") else {
            completion(.failure(ResultError.NetworkError))
            return
        }
        // using URLSession to fetch data from API
        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let _ = error {
                completion(.failure(ResultError.NetworkError))
            }
            guard let data = data else {
                completion(.failure(ResultError.NetworkError))
                return
            }
            // parsing school SAT detail if it's success returns or error happens return error type
            do {
                // result will be array type, need to check array is empty or not
                let entities = try JSONDecoder().decode([SATResult].self, from: data)
                if entities.count > 0 {
                    completion(.success(entities[0]))
                } else {
                    completion(.success(nil))
                }
            } catch {
                completion(.failure(ResultError.ParsingError))
            }
        }
        task.resume()
    }
    
}
