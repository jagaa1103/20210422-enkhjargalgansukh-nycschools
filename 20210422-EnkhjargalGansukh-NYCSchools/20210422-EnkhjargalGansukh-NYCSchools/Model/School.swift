//
//  School.swift
//  20210422-EnkhjargalGansukh-NYCSchools
//
//  Created by Enkhjargal Gansukh on 2021.04.22.
//

import Foundation

/*
    School Model to parsing JSON
*/
struct School: Decodable {
    let dbn: String
    let school_name: String
    let primary_address_line_1: String
    let city: String
    let zip: String
}
