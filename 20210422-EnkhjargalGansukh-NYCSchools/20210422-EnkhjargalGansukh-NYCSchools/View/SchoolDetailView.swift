//
//  SchoolDetailView.swift
//  20210422-EnkhjargalGansukh-NYCSchools
//
//  Created by Enkhjargal Gansukh on 2021.04.22.
//

import Foundation
import UIKit

/*
 SchoolDetailView is responsible for rendering and handling UI
*/
class SchoolDetailView: UIViewController, UINavigationBarDelegate, SchoolDetailProtocol {
    
    // variables
    let viewModel = SchoolDetailViewModel()
    var school: School?
    
    // UI declarations
    var nameLabel = UILabel()
    var satMath = UILabel()
    var satWriting = UILabel()
    var satReading = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        viewModel.delegate = self
        if let dbn = school?.dbn {
            DispatchQueue.main.async {
                self.viewModel.getSchoolDetail(dbn: dbn)
            }
        }
        setNavBar()
        setLabels()
    }
    
    // rendering Tab Bar
    func setNavBar() {
        let navItem = UINavigationItem()
        let closeBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(closeView))
        navItem.rightBarButtonItems = [closeBtn]
        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 75))
        navBar.backgroundColor = .systemBackground
        navBar.delegate = self
        navBar.items = [navItem]
        view.addSubview(navBar)
    }
    
    // close button event handle
    @objc func closeView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    // when error happened showing alert view
    func showAlert(error: Error) {
        var message = "Error! Please restart application."
        switch error {
            case ResultError.NetworkError:
                message = "Network error! Please check your connection."
                break
            case ResultError.ParsingError:
                message = "Parsing error! Please try again."
                break
            default:
                break
        }
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // protocol method
    func fetched() {
        setLabels()
    }
    
    // setting and rendering UI labels
    func setLabels() {
        nameLabel.text = school?.school_name
        if let data = viewModel.detailData {
            satMath.text = "SAT Math: \(data.sat_math_avg_score)"
            satWriting.text = "SAT Writing: \(data.sat_writing_avg_score)"
            satReading.text = "SAT Reading: \(data.sat_critical_reading_avg_score)"
        } else {
            satMath.text = "SAT Math: --"
            satWriting.text = "SAT Writing: --"
            satReading.text = "SAT Reading: --"
        }
        nameLabel.font = .systemFont(ofSize: 24, weight: .semibold)
        satMath.font = .systemFont(ofSize: 24)
        satWriting.font = .systemFont(ofSize: 24)
        satReading.font = .systemFont(ofSize: 24)
        
        // stack view with number labels
        let stack = UIStackView(arrangedSubviews: [nameLabel, satMath, satWriting, satReading])
        stack.alignment = .center
        stack.distribution = .equalSpacing
        stack.axis = .vertical
        view.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        stack.heightAnchor.constraint(equalToConstant: 200).isActive = true
        stack.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        stack.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
