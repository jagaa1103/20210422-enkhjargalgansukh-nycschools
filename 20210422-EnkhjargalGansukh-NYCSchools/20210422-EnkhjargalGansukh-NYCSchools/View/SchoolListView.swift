//
//  SchoolListView.swift
//  20210422-EnkhjargalGansukh-NYCSchools
//
//  Created by Enkhjargal Gansukh on 2021.04.22.
//

import Foundation
import UIKit

/*
 SchoolListView is responsible for rendering and handling UI
*/
class SchoolListView: UIViewController {
    
    // variables
    let viewModel = SchoolListViewModel()
    var tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        setNavBar()
        setTableView()
        viewModel.getSchoolList()
    }
    
    // rendering Navigation Bar
    func setNavBar() {
        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100))
        navBar.prefersLargeTitles = true
        let title = UINavigationItem(title: "Schools")
        navBar.backgroundColor = .systemBackground
        navBar.items = [title]
        view.addSubview(navBar)
    }
    
    // rendering Table View
    func setTableView() {
        tableView.frame = CGRect(x: 0, y: 100, width: view.bounds.width, height: view.bounds.height - 100)
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
}

// extending SchoolListView by protocol
extension SchoolListView: SchoolListProtocol {
    func fetched() {
        // when data fetched need to reload data in tableview
        tableView.reloadData()
    }
}

// extending SchoolListView by TableView delegation
extension SchoolListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = viewModel.schools[indexPath.row].school_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailView = SchoolDetailView()
        detailView.school = viewModel.schools[indexPath.row]
        self.present(detailView, animated: true, completion: nil)
    }
}
